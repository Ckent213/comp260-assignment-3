﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObstacleHit : MonoBehaviour {
	public LayerMask PlayerLayer;
	public LayerMask ZombiesLayer;
	public LayerMask DestroyerLayer;
	private AudioSource _audio;
	private bool isGone = false;

	public Text gameOver;

	public void Start() {
		_audio = gameObject.GetComponent<AudioSource>();
	}

	public void OnTriggerEnter2D(Collider2D collider) {		
		if (isGone) return;

		// check if the colliding object is in the zombieLayer
		if (((1 << collider.gameObject.layer) & ZombiesLayer.value) != 0) {
			// play a sound if there is one
			if (_audio != null) {
				_audio.Play();
			}

			// hide the sprite and disable this script
			Destroy(gameObject);
		}

		// check if the colliding object is in the playerLayer
		if (((1 << collider.gameObject.layer) & PlayerLayer.value) != 0) {
			// play a sound if there is one
			if (_audio != null) {
				_audio.Play();
			}
				
			Time.timeScale -= 0.05f; 
			Time.timeScale = 0.0f; 
			Time.timeScale -= 0.05f; 
			Destroy (GameObject.Find("TagSpawner"));
			Destroy (GameObject.Find("ObstacleSpawner"));
			Destroy (GameObject.Find("FloorSpawner"));
			gameOver.enabled = true;
	
		}
	}

}
